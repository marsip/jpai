package itakademija.java2015.jpa.demo2;

import java.time.LocalDateTime;
import java.time.Month;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import itakademija.java2015.jpa.assigment1.entities.Address;
import itakademija.java2015.jpa.assigment1.entities.Author;
import itakademija.java2015.jpa.assigment1.entities.Book;
import itakademija.java2015.jpa.assigment1.entities.Genre;

/**
 * This class is responsible for building objects for
 * use in application or in tests.
 */
public class BookDbDataBuildHelper {

	public static Book buildSampleBook1() {
		Book book = new Book();
		LocalDateTime releaseDate = LocalDateTime.of(2008, Month.MAY, 28, 0, 0);
		Date convertedDate = Date.from(releaseDate.atZone(ZoneId.systemDefault()).toInstant());
		book.setReleaseDate(convertedDate);
		book.setTitle("Effective Java");
		book.setIsbn("1234567891011");
		
		List<Genre> genres = new ArrayList<Genre>();
		genres.add(buildSampleGenre1());
		genres.add(buildSampleGenre2());
		book.setGenres(genres);
		
		Address add1 = buildAddress();
		
		Author author = new Author();
		author.setName("Joshua");
		author.setLastname("Bloch");
		author.setAddress(add1);
		
		
		book.setAuthor(author);
		
		return book;
	}

	public static Address buildAddress() {
		Address add1 = new Address();
		add1.setBuilding("11");
		add1.setCity("Vilnius");
		add1.setCountry("LT");
		add1.setFlat("0");
		return add1;
	}

	public static Book buildSampleBook2() {
		LocalDateTime releaseDate;
		Date convertedDate;
		Author author;
		Book book;
		author = new Author();
		author.setName("Javier");
		author.setLastname("Fernández");
		author.setAddress(buildAddress());
		
		releaseDate = LocalDateTime.of(2012, Month.OCTOBER, 25, 0, 0);
		convertedDate = Date.from(releaseDate.atZone(ZoneId.systemDefault()).toInstant());
		book = new Book();
		book.setTitle("Java 7 Concurrency Cookbook");
		book.setAuthor(author);
		book.setReleaseDate(convertedDate);
		book.setIsbn("1213141516171");
		
		List<Genre> genres = new ArrayList<Genre>();
		genres.add(buildSampleGenre1());
		genres.add(buildSampleGenre3());
		book.setGenres(genres);
		
		return book;
	}
	
	public static Genre buildSampleGenre1() {
		Genre genre = new Genre();
		genre.setName("detective");
		genre.setBooks(new ArrayList<Book>());
		return genre;
	}

	public static Genre buildSampleGenre2() {
		Genre genre = new Genre();
		genre.setName("fiction");
		genre.setBooks(new ArrayList<Book>());
		return genre;
	}

	public static Genre buildSampleGenre3() {
		Genre genre = new Genre();
		genre.setName("biography");
		genre.setBooks(new ArrayList<Book>());
		return genre;
	}

}
