package itakademija.java2015.jpa.demo1.repositories;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import itakademija.java2015.jpa.assigment1.entities.Book;
import itakademija.java2015.jpa.assigment1.entities.Book_;
import itakademija.java2015.jpa.assigment1.entities.Genre;
import itakademija.java2015.jpa.assigment1.entities.Genre_;

public class BookRepositoryJPA implements BookRepository {
	
	private EntityManager em;

	public BookRepositoryJPA(EntityManager em) {
		this.em = em;
	}

	@Override
	public void insertOrUpdate(Book book) {
		em.getTransaction().begin();
		em.persist(book);
		em.getTransaction().commit();
	}

	@Override
	public void delete(Book book) {
		em.getTransaction().begin();
		em.remove(book);
		em.getTransaction().commit();
	}

	@Override
	public void deleteById(Long bookId) {
		em.getTransaction().begin();
		Book book = em.find(Book.class, bookId);
		if (book != null)
			em.remove(book);
		em.getTransaction().commit();
	}

	@Override
	public List<Book> findByTitleFragment(String titleFragment) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Book> cq = cb.createQuery(Book.class);
		Root<Book> bookRoot = cq.from(Book.class);
		cq.select(bookRoot); // we select entity here
		/**
		 * Construct SQL in typesafe way
		 * should result in "SELECT ... FROM book WHERE lower(title) like lower('%blahblah%')" 
		 */
		cq.where(cb.like(cb.lower(bookRoot.get(Book_.title)), "%" + titleFragment.toLowerCase() + "%")); 
		TypedQuery<Book> q = em.createQuery(cq);
		return q.getResultList();
	}
	@Override
	public Long countAllBooks() {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Long> countQuery = cb.createQuery(Long.class);
		
		countQuery.select(cb.count(countQuery.from(Book.class)));
		TypedQuery<Long> q = em.createQuery(countQuery);
		
		return q.getSingleResult();
	}
	
	/**
	 * Implementation of BookRepository interface method findByIsbn
	 */
	@Override
	public Book findByIsbn(String isbn) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Book> cq = cb.createQuery(Book.class);
		Root<Book> bookRoot = cq.from(Book.class);
		cq.select(bookRoot); // we select entity here
		/**
		 * Construct SQL in typesafe way
		 * should result in "SELECT ... FROM book WHERE isbn = 'String up to 13 symbols'" 
		 */
		cq.where(cb.equal(bookRoot.get(Book_.isbn), isbn)); 
		TypedQuery<Book> q = em.createQuery(cq);
		return q.getSingleResult();
	}

	/**
	 * Implementation of BookRepository interface method findBooksByGenre
	 */
	@Override
	public List<Book> findBooksByGenre(Genre genre) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Genre> cq = cb.createQuery(Genre.class);
		Root<Genre> genreRoot = cq.from(Genre.class);
		cq.select(genreRoot); // we select entity here
		cq.where(cb.equal(genreRoot.get(Genre_.id), genre.getId())); 
		TypedQuery<Genre> q = em.createQuery(cq);
		return q.getSingleResult().getBooks();
	}

}
