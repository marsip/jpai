package itakademija.java2015.jpa.demo1.repositories;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import itakademija.java2015.jpa.assigment1.entities.Book;
import itakademija.java2015.jpa.assigment1.entities.Book_;
import itakademija.java2015.jpa.assigment1.entities.Genre;

public class GenreRepositoryJPA implements GenreRepository {

	private EntityManager em;
	
	public GenreRepositoryJPA(EntityManager em) {
		this.em = em;
	}

	@Override
	public List<Genre> findGenresForGivenBook(Book book) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Book> cq = cb.createQuery(Book.class);
		Root<Book> bookRoot = cq.from(Book.class);
		cq.select(bookRoot);
		cq.where(cb.equal(bookRoot.get(Book_.id), book.getId())); 
		TypedQuery<Book> q = em.createQuery(cq);
		return q.getSingleResult().getGenres();
	}

	@Override
	public void insertOrUpdate(Genre genre) {
		em.getTransaction().begin();
		em.persist(genre);
		em.getTransaction().commit();
	}

}
