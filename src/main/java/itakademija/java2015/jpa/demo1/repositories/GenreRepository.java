package itakademija.java2015.jpa.demo1.repositories;

import java.util.List;

import itakademija.java2015.jpa.assigment1.entities.Book;
import itakademija.java2015.jpa.assigment1.entities.Genre;

public interface GenreRepository {
	public void insertOrUpdate(Genre genre);
	/*
	 * Abstract method to find all genres for given book
	 */
	public List<Genre> findGenresForGivenBook(Book book);
}
