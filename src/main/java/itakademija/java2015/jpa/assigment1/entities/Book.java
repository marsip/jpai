package itakademija.java2015.jpa.assigment1.entities;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;


@Entity
public class Book {
	@Id
	@GeneratedValue
	private Long id;
	@NotNull
	private String title;
	@Temporal(TemporalType.DATE)
	private Date releaseDate;
	/**
	 * Edition of the book; For example: "second", "2nd", "anniversary", ..
	 */
	private String edition;
	@JoinColumn(name="author_id")
	@ManyToOne(optional=true, cascade={CascadeType.ALL})
	private Author author;
	
	/**
	 * Knygos ISBN
	 */
	@Column(unique=true, length=13)
	private String isbn;
	
	/**
	 * Getter for isbn field
	 * @return
	 */
	public String getIsbn() {
		return isbn;
	}
	/**
	 * Setter for isbn field
	 * @param isbn
	 */
	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}
	
	/**
	 * Knygos žanras
	 */
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name="book_genre", 
		joinColumns=@JoinColumn(name="book_id", referencedColumnName = "id"), 
		inverseJoinColumns=@JoinColumn(name="genre_id", referencedColumnName = "id"))
	private List<Genre> genres;
	
	/**
	 * Getteris knygos žanrui
	 * @return
	 */
	public List<Genre> getGenres() {
		return genres;
	}
	/**
	 * Setteris knygos žanrui
	 * @param genres
	 */
	public void setGenres(List<Genre> genres) {
		this.genres = genres;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Date getReleaseDate() {
		return releaseDate;
	}

	public void setReleaseDate(Date releaseDate) {
		this.releaseDate = releaseDate;
	}

	public String getEdition() {
		return edition;
	}

	public void setEdition(String edition) {
		this.edition = edition;
	}

	public Author getAuthor() {
		return author;
	}

	public void setAuthor(Author author) {
		this.author = author;
		author.addBook(this);
	}

	@Override
	public String toString() {
		return "Book [isbn= " + isbn +", title=" + title + ", releaseDate=" + releaseDate + ", edition=" + edition + ", author=" + author
				+ "]";
	}
}
