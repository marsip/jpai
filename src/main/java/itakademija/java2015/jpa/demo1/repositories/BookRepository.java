package itakademija.java2015.jpa.demo1.repositories;

import java.util.List;

import itakademija.java2015.jpa.assigment1.entities.Book;
import itakademija.java2015.jpa.assigment1.entities.Genre;

public interface BookRepository {
	/*
	 * If book.id is set then update operation is performed
	 * If book.id == null then insert operation is performed
	 */
	public void insertOrUpdate(Book book);
	public void delete(Book book);
	public void deleteById(Long bookId);
	public List<Book> findByTitleFragment(String titleFragment);
	Long countAllBooks();
	/*
	 * abstract method to find a book by isbn
	 */
	public Book findByIsbn(String isbn);
	/*
	 * abstract method to find a books by genre
	 */
	public List<Book> findBooksByGenre(Genre genre);
}
