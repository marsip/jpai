package itakademija.java2015.jpa.demo1;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import itakademija.java2015.db.utils.ConnectionUtils;
import itakademija.java2015.jpa.demo1.entities.SmsMessage;

public class Demo1 {

	static final Logger log = LoggerFactory.getLogger(Demo1.class);

	public static void main(String[] args) {
		new Demo1().execute();

	}

	private void execute() {
		log.info("start");

		try (Connection con = ConnectionUtils.establishConnection()) {
			loadFromDb(con);

			SmsMessage msg = recieveNextMessage();
			
			saveToDb(msg, con);
			
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	private void loadFromDb(Connection con) {
		try (Statement stmt = con.createStatement()) {
			ResultSet result = stmt.executeQuery("SELECT id, text, sendernumber, recipientnumber FROM SMSMESSAGEJPA");

			log.info("Current messages:");
			while (result.next()) {
				SmsMessage sms = new SmsMessage();

				long id = result.getLong("id");
				sms.setId(id);
				sms.setText(result.getString("text"));
				sms.setSenderNumber(result.getString("sendernumber"));
				sms.setRecipientNumber(result.getString("recipientnumber"));

				log.info("sms: {}", sms);

			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	private void saveToDb(SmsMessage msg, Connection con) {
		try (Statement stmt = con.createStatement()) {
							
				try (PreparedStatement statement = con
						.prepareStatement("INSERT INTO SMSMESSAGEJPA (sendernumber, recipientnumber, text, id) values (?, ?, ?, NEXT VALUE FOR HIBERNATE_SEQUENCE)")) {
					statement.setString(1, msg.getSenderNumber());
					statement.setString(2, msg.getRecipientNumber());
					statement.setString(3, msg.getText());

					statement.execute();
				} catch (SQLException e) {
					throw new RuntimeException(e);
				}

		} catch (SQLException e) {
			throw new RuntimeException(e);
		}

	}

	private static SmsMessage recieveNextMessage() {

		SmsMessage msg = new SmsMessage();
		msg.setSenderNumber("990238423984");
		msg.setRecipientNumber("242302342309");
		msg.setText("labas rytas");
		return msg;
	}
}
